# Environment Variables Filler 

A command line tool that generates a local environment variables and/or fills an Elastic Beanstalk Environment with Environment Variables
It handles json and csv files

## Getting Started

NPM package is a work in progress. Now just clone the repo 

### Installing

Switch to the directory of the cloned repository 

```
npm install // To install dependencies
npm install -g // To install this module globally 
```
### Usage

To use this package and understand how it works run the following command

~~~
env-cli -h 
~~~


## Built With

* [Commander](https://github.com/tj/commander.js/) - Tooling to create CLI tools with node.js
* [aws-sdk](https://aws.amazon.com/sdk-for-node-js/) - SDK for managing AWS resources

## Authors

* **Qais Aboujaoude** 
* **Connor Makhlouta** 

## License

This project is licensed under the Mozilla Public License MPL  License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks to Hassan Assi for all the love and support